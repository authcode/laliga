import React from 'react';
import styled from 'styled-components';
import { CardProps } from './types.d';

const StyledCard = styled.div<CardProps>`
  background-color: ${(props) => props.theme.white};
  border-radius: ${(props) => props.theme.base_radius};
  width: ${(props) =>
    props.width ? `${props.width}px` : `calc(100% - ${props.noPadding ? '0px' : '40px'})`};
  max-width: ${(props) => (props.maxWidth ? `${props.maxWidth}px` : 'auto')};
  padding: ${(props) => (props.noPadding ? '0' : '20px')};
  margin-bottom: 20px;
  box-shadow: ${(props) => props.theme.base_shadow};
  overflow: hidden;
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    padding: 15px 20px;
    margin-bottom: ${(props) => (props.noPadding ? '0' : '20px')};
    margin-top: ${(props) => (props.noPadding ? '0' : '-20px')};
    margin-left: ${(props) => (props.noPadding ? '0' : '-20px')};
    margin-right: ${(props) => (props.noPadding ? '0' : '-20px')};
    background-color: ${(props) => props.theme.white};
    color: ${(props) => props.theme.black};
    border-bottom: 1px solid ${(props) => props.theme.gray};
    border-top-left-radius: ${(props) => props.theme.base_radius};
    border-top-right-radius: ${(props) => props.theme.base_radius};
    svg {
      margin-right: 8px;
    }
  }
  @media screen and (max-width: 767px) {
    width: ${(props) => (props.noPadding ? '100%' : 'calc(100% - 40px)')};
    max-width: auto;
  }
`;

const Card: React.FC<CardProps> = ({ children, title, ...props }) => {
  return (
    <StyledCard {...props}>
      {title}
      {children}
    </StyledCard>
  );
};

export default Card;
