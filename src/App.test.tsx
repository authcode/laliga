import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

describe('Render app', () => {
  it('renders the login component', () => {
    render(<App />);
    const button = screen.getByText(/Acceder/i);
    expect(button).toBeInTheDocument();
  });

  it('renders the footer', () => {
    render(<App />);
    const rights = screen.getByText(/LaLiga/i);
    expect(rights).toBeInTheDocument();
  });
});
