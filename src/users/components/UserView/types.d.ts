import { User } from '../../types/users.d';

export interface UserViewParams {
  readonly user_id?: string;
}

export type UserFormValues = Pick<User, 'first_name' | 'last_name' | 'email'>;
