export const USERS_ROUTE = '/users';
export const USERS_PAGE_ROUTE = `${USERS_ROUTE}/page/:currentPage`;
export const USERS_EDIT_ROUTE = `${USERS_ROUTE}/:user_id`;
