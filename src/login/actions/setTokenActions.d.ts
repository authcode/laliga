import { Action } from 'redux';

export const SET_TOKEN = 'login/set_token';
export const DELETE_TOKEN = 'login/delete_token';

export interface SetTokenAction extends Action<typeof SET_TOKEN> {
  readonly token: string;
}

export type DeleteTokenAction = Action<typeof DELETE_TOKEN>;

export type SetTokenActions = SetTokenAction | DeleteTokenAction;
