import { RouteComponentProps } from 'react-router-dom';

export type PrivateRouteProps = RouteComponentProps & {
  readonly path: string;
};
