import { CSSProperties } from 'react';
export interface AlertProps {
  readonly type?: 'info' | 'success' | 'warning' | 'danger';
  readonly style?: CSSProperties;
}
