import { Reducer } from 'redux';
import {
  GET_USERS_CALLED,
  GET_USERS_SUCEEDED,
  GET_USERS_FAILED,
} from '../actions/getUsersActions.d';
import { UserListState } from '../states/usersListState.d';
import { UsersReducerActions } from './usersReducer.d';

const initialState: UserListState = {
  loading: false,
  page: 1,
  per_page: 6,
  total: 0,
  total_pages: 0,
  data: undefined,
  error: undefined,
};

const usersReducer: Reducer<UserListState, UsersReducerActions> = (
  state = initialState,
  action,
): UserListState => {
  switch (action.type) {
    case GET_USERS_CALLED: {
      return {
        ...state,
        error: undefined,
        loading: true,
      };
    }
    case GET_USERS_SUCEEDED: {
      return {
        ...state,
        ...action.usersList,
        error: undefined,
        loading: false,
      };
    }
    case GET_USERS_FAILED: {
      return {
        ...state,
        error: action.error,
        loading: false,
      };
    }
    default:
      return state;
  }
};

export default usersReducer;
