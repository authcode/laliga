import { LoginFormValues } from '../types/login.d';
import { LOGIN_CALLED } from './loginActions.d';

export function login(payload: LoginFormValues) {
  return {
    type: LOGIN_CALLED,
    payload,
  };
}
