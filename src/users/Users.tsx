import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { UsersHeader, UsersList, UserView } from './components';
import { USERS_EDIT_ROUTE, USERS_PAGE_ROUTE } from './routes';

const Users: React.FC = () => {
  return (
    <>
      <UsersHeader />
      <Router>
        <Switch>
          <Route path={USERS_PAGE_ROUTE}>
            <UsersList />
          </Route>
          <Route path={USERS_EDIT_ROUTE}>
            <UserView />
          </Route>
          <Route path="*">
            <Redirect to={USERS_PAGE_ROUTE.replace(':currentPage', '1')} />
          </Route>
        </Switch>
      </Router>
    </>
  );
};

export default Users;
