# 📝 Technical Test - User administration panel

A user administration panel made with [create-react-app](https://github.com/facebook/create-react-app) using TypeScript and React for technical test purpose. Read full documentation [here](https://gitlab.com/authcode/laliga/-/blob/master/documentacion_prueba_tecnica_laliga.pdf).

## ✅ Requirements

You only need [Node.js](https://nodejs.org/), recommended LTS version. If you prefer, you can optionally use [yarn](https://yarnpkg.com/) as a package manager.

## 👩‍💻 Get started

1. Clone this repo

```
> git clone git@gitlab.com:authcode/laliga.git
```

2. Install dependencies

```
> cd laliga
> npm install
```

3. Start the server

```
> npm start
```

4. This last command will automatically open a tab in your browser with this address: [http://localhost:3000](http://localhost:3000)

5. To login into the administration panel use this credentials:

```
E-Mail: eve.holt@reqres.in
Password: cityslicka
```

## 🔬 Run tests

To run application tests use this command:

```
> npm run test
```

## 🚀 Build & Deploy

To build project for distribution use this command:

```
> npm run build
```

To deploy the application upload the files contained in the build folder to a production server

## 🛠️ Customize configuration

You can configure some server properties using the **.env** file (change API Rest endpoint, port, etc.).

## 📖 License

This project is under [MIT](https://opensource.org/licenses/MIT).

Some logos used for the interface belong to [La Liga Nacional de Futbol Profesional](https://www.laliga.com/informacion-legal/legal-web).

The background image used are under [FreePik license](https://www.freepik.es/vectores/luz) for commercial and personal use.
