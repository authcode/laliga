import { GetUsersActions } from '../actions/getUsersActions.d';

export type UsersReducerActions = GetUsersActions;
