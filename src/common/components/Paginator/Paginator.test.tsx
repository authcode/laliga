import React, { useState } from 'react';
import { fireEvent, render } from '@testing-library/react';
import Paginator from './Paginator';

function TestPaginator() {
  const [text, setText] = useState<string>('Page 2');

  const handlePaginate = (newPage: number) => {
    setText(`Page ${newPage}`);
  };

  return (
    <>
      <div>{text}</div>
      <Paginator
        page={2}
        totalPages={3}
        totalItems={9}
        itemsName="usuario"
        onPaginate={handlePaginate}
        aria-label="paginator"
      />
    </>
  );
}

const setup = () => {
  const component = render(<TestPaginator />);
  return {
    ...component,
  };
};

describe('Paginator component', () => {
  test('There must be 3 buttons', () => {
    const { getAllByRole } = setup();
    const buttons = getAllByRole('button');
    expect(buttons.length).toBe(3);
  });

  test('It should change the page', () => {
    const { getAllByRole, getByText } = setup();
    const button = getAllByRole('button').pop();

    if (!button) return false;
    fireEvent.click(button);

    const text = getByText('Page 3');
    expect(text).toBeInTheDocument();
  });
});
