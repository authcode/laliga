import { apiPost } from '../../common/services';
import { ResponseError } from '../../common/types';
import { LoginResponse } from '../types/login';

const LOGIN_URL = `login`;

export function login(email: string, password: string): Promise<LoginResponse & ResponseError> {
  return apiPost<LoginResponse>(LOGIN_URL, { email, password });
}
