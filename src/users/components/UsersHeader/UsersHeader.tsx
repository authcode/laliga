import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';
import { Button } from '../../../common/components';
import { useAuth } from '../../../common/hooks';
import { deleteContextToken } from '../../../login/actions/setTokenActions';
import logo from '../../../assets/logo-horizontal.svg';

const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 25px;
  img {
    width: 100%;
    max-width: 200px;
  }
  @media screen and (max-width: 767px) {
    padding: 25px 0px;
    img {
      max-width: 150px;
    }
  }
`;

const UsersHeader: React.FC = () => {
  const dispatch = useDispatch();
  const { deleteToken } = useAuth();
  const history = useHistory();

  function handleLogout() {
    deleteToken();
    dispatch(deleteContextToken());
    history.replace('/login');
  }

  return (
    <StyledHeader>
      <div>
        <img src={logo} alt="LaLiga" />
      </div>
      <Button type="button" color="danger" onClick={handleLogout} sm>
        <FontAwesomeIcon icon={faSignOutAlt} />
        Cerrar sesión
      </Button>
    </StyledHeader>
  );
};

export default UsersHeader;
