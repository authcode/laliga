import React from 'react';
import { render, screen } from '@testing-library/react';
import Card from './Card';

test('Render card', () => {
  render(<Card title={<h2>Card title</h2>}>Card body</Card>);
  const title = screen.getByText(/Card title/i);
  const body = screen.getByText(/Card body/i);

  expect(title).toBeInTheDocument();
  expect(body).toBeInTheDocument();
});
