import { all } from 'redux-saga/effects';
import loginUserSaga from '../../login/sagas/loginSaga';
import usersSaga from '../../users/sagas/usersSaga';

export default function* rootSaga() {
  yield all([loginUserSaga(), usersSaga()]);
}
