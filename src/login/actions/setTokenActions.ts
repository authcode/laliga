import { DELETE_TOKEN, SET_TOKEN } from './setTokenActions.d';

export function setContextToken(token: string) {
  return {
    type: SET_TOKEN,
    token,
  };
}

export function deleteContextToken() {
  return {
    type: DELETE_TOKEN,
  };
}
