import { InputHTMLAttributes } from 'react';

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  readonly full?: boolean;
  readonly label?: string;
  readonly error?: string;
}
