import React from 'react';
import styled from 'styled-components';
import { Button } from '..';
import { PaginatorProps } from './types';

const StyledPaginator = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: ${(props) => props.theme.white};
  text-shadow: ${(props) => props.theme.base_text_shadow};
  margin-bottom: 25px;
`;

const Paginator: React.FC<PaginatorProps> = ({
  page,
  totalPages,
  totalItems,
  itemsName,
  onPaginate,
}) => {
  return (
    <StyledPaginator>
      <div>
        Total: {totalItems} {itemsName || 'elemento'}
        {totalItems > 1 && 's'}
      </div>
      <div>
        <div style={{ display: 'inline', marginRight: '5px' }}>Páginas:</div>
        {Array.from(new Array(totalPages), (_, p) => (
          <Button
            key={p + 1}
            type="button"
            color={p + 1 === page ? 'primary' : 'default'}
            onClick={() => onPaginate(p + 1)}
            disabled={p + 1 === page}
            sm
            mx
          >
            {p + 1}
          </Button>
        ))}
      </div>
    </StyledPaginator>
  );
};

export default Paginator;
