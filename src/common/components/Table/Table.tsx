import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { TableData, TableProps } from './types.d';

const StyledCard = styled.table<Partial<TableProps>>`
  width: 100%;
  margin: 0;
  border-collapse: collapse;
  th {
    background-color: ${(props) => props.theme.white_2};
    color: ${(props) => props.theme.black};
    font-size: ${(props) => props.theme.font_md};
    padding: 0.5em 1em;
  }
  td {
    font-size: ${(props) => props.theme.font_md};
    padding: 0.5em 1em;
  }
  tbody tr:nth-child(2n) {
    background-color: ${(props) => props.theme.white_1};
  }
  @media screen and (max-width: 991px) {
    .no-tablet {
      display: none;
    }
  }
  @media screen and (max-width: 767px) {
    .no-mobile {
      display: none;
    }
  }
`;

const Table: React.FC<TableProps> = ({ columns, data, ...props }) => {
  function getColumnData(row: TableData, field: string): ReactNode {
    if (field in row) return row[field] as ReactNode;
    return '';
  }

  return (
    <StyledCard {...props}>
      <thead>
        <tr>
          {columns.map((column) => (
            <th
              key={column.field}
              className={column.noTablet ? 'no-tablet' : column.noMobile ? 'no-mobile' : ''}
              style={{ textAlign: column.align || 'left' }}
            >
              {column.title}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((row, i) => (
          <tr key={`row-${i}`}>
            {columns.map((column) => (
              <td
                key={`${column.field}-${i}`}
                className={column.noTablet ? 'no-tablet' : column.noMobile ? 'no-mobile' : ''}
                style={{ textAlign: column.align || 'left' }}
              >
                {getColumnData(row, column.field)}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </StyledCard>
  );
};

export default Table;
