import React from 'react';
import { render, screen } from '@testing-library/react';
import Table from './Table';

const columns = [
  { field: 'id', title: 'ID' },
  { field: 'name', title: 'Name' },
];

const data = [
  { id: 1, name: 'User 1' },
  { id: 2, name: 'User 2' },
  { id: 3, name: 'User 3' },
];

test('Render table', () => {
  render(<Table columns={columns} data={data} />);

  const text = screen.getByText('User 2');
  const numCols = screen.getAllByRole('columnheader');
  const numRows = screen.getAllByRole('row');

  expect(text).toBeInTheDocument();
  expect(numCols.length).toBe(2);
  expect(numRows.length).toBe(4);
});
