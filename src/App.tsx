import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import store from './common/store';
import theme from './common/theme';
import Login from './login/Login';
import { Footer, PrivateRoute } from './common/components';
import Users from './users/Users';
import { LOGIN_ROUTE } from './login/routes';
import { USERS_ROUTE } from './users/routes';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <main>
          <Router>
            <Switch>
              <Route path={LOGIN_ROUTE}>
                <Login />
              </Route>
              <PrivateRoute path={USERS_ROUTE}>
                <Users />
              </PrivateRoute>
              <Route path="*">
                <Redirect to={LOGIN_ROUTE} />
              </Route>
            </Switch>
          </Router>
          <Footer />
        </main>
      </ThemeProvider>
      <div className="background"></div>
    </Provider>
  );
};

export default App;
