import { Reducer } from 'redux';
import { LOGIN_CALLED, LOGIN_FAILED, LOGIN_SUCEEDED } from '../actions/loginActions.d';
import { SET_TOKEN, DELETE_TOKEN } from '../actions/setTokenActions.d';
import { LoginState } from '../states/loginState.d';
import { LoginReducerActions } from './loginReducer.d';

const initialState: LoginState = {
  isLoading: false,
  token: undefined,
};

const loginReducer: Reducer<LoginState, LoginReducerActions> = (
  state = initialState,
  action,
): LoginState => {
  switch (action.type) {
    case LOGIN_CALLED: {
      return {
        ...state,
        error: undefined,
        isLoading: true,
      };
    }
    case LOGIN_SUCEEDED: {
      return {
        ...state,
        ...action.loginState,
        error: undefined,
        isLoading: false,
      };
    }
    case LOGIN_FAILED: {
      return {
        ...state,
        error: action.error,
        token: undefined,
        isLoading: false,
      };
    }
    case SET_TOKEN: {
      return {
        ...state,
        token: action.token,
      };
    }
    case DELETE_TOKEN: {
      return {
        ...state,
        token: undefined,
      };
    }
    default:
      return state;
  }
};

export default loginReducer;
