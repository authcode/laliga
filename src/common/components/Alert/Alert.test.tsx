import React from 'react';
import { render, screen } from '@testing-library/react';
import Alert from './Alert';

test('Render alert', () => {
  render(<Alert type="info">Testing alert</Alert>);
  const alertText = screen.getByText(/Testing alert/i);
  expect(alertText).toBeInTheDocument();
});
