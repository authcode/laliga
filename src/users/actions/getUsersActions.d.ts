import { Action } from 'redux';
import { UserListState } from '../components/UsersList/types.d';

export const GET_USERS_CALLED = 'users/get_users_called';
export const GET_USERS_SUCEEDED = 'users/get_users_suceeded';
export const GET_USERS_FAILED = 'users/get_users_failed';

export interface GetUsersCalledAction extends Action<typeof GET_USERS_CALLED> {
  readonly page: number;
}

export interface GetUsersSuceededAction extends Action<typeof GET_USERS_SUCEEDED> {
  readonly usersList: UserListState;
}

export interface GetUsersFailedAction extends Action<typeof GET_USERS_FAILED> {
  readonly error: string;
}

export type GetUsersActions = GetUsersCalledAction | GetUsersSuceededAction | GetUsersFailedAction;
