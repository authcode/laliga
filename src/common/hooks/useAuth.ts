function useAuth() {
  const AUTH_TOKEN = process.env.REACT_APP_AUTH_TOKEN || 'authToken';
  const getToken = () =>
    window?.sessionStorage ? window.sessionStorage.getItem(AUTH_TOKEN) || undefined : undefined;
  const deleteToken = () => window?.sessionStorage && window.sessionStorage.removeItem(AUTH_TOKEN);
  const setToken = (token?: string) => {
    token !== undefined && window?.sessionStorage
      ? window.sessionStorage.setItem(AUTH_TOKEN, token)
      : deleteToken();
  };

  return {
    getToken,
    setToken,
    deleteToken,
  };
}

export default useAuth;
