export const UNEXPECTED_ERROR = 'Error inesperado';
export const REQUIRED_FIELD = 'Dato requerido';
export const FORMAT_EMAIL = 'Debe ser un e-mail válido';
