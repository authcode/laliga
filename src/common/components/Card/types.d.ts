import { CSSProperties, ReactNode } from 'react';

export interface CardProps {
  readonly width?: number;
  readonly maxWidth?: number;
  readonly style?: CSSProperties;
  readonly title?: ReactNode;
  readonly noPadding?: boolean;
}
