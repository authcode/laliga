import { SagaIterator } from 'redux-saga';
import { call, put, takeEvery } from 'redux-saga/effects';
import {
  LoginCalledAction,
  LOGIN_CALLED,
  LOGIN_FAILED,
  LOGIN_SUCEEDED,
} from '../actions/loginActions.d';
import { login } from '../services';

function* loginUser(action: LoginCalledAction): SagaIterator {
  const { username, password } = action.payload;
  const result = yield call(login, username, password);
  if ('error' in result) {
    yield put({ type: LOGIN_FAILED, error: result.error });
  } else {
    yield put({
      type: LOGIN_SUCEEDED,
      loginState: { token: result.token },
    });
  }
}

function* loginUserSaga() {
  yield takeEvery(LOGIN_CALLED, loginUser);
}

export default loginUserSaga;
