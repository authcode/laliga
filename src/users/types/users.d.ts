export interface User {
  readonly id: number;
  readonly email: string;
  readonly first_name: string;
  readonly last_name: string;
  readonly avatar: string;
}

export interface UserList {
  readonly page: number;
  readonly per_page: number;
  readonly total: number;
  readonly total_pages: number;
  readonly data?: Readonly<User[]>;
}

export interface UserResponse {
  readonly data: User;
}

export interface UserUpdateResponse extends User {
  readonly updatedAt: string;
}
