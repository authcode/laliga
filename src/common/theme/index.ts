const theme = {
  // Colors
  white: '#ffffff',
  white_1: '#f8f8f8',
  white_2: '#ebebeb',
  default: '#ebebeb',
  default_hover: '#c0c0c0',
  primary: '#0090d7',
  primary_hover: '#01509f',
  primary_soft: '#b3cedb',
  success: '#00963f',
  success_hover: '#007f35',
  success_soft: '#b8e0c8',
  warning: '#f28800',
  warning_hover: '#c87000',
  warning_soft: '#ecdca4',
  danger: '#e40613',
  danger_hover: '#c80813',
  danger_soft: '#ff999f',
  black: '#080808',
  gray: '#787878',

  // Fonts
  base_font: `'Roboto', sans-serif`,
  font_md: '0.9em',
  font_sm: '0.8em',
  font_xs: '0.6em',

  // Paddings:
  button_padding: '0.5em 1rem',
  button_padding_sm: '0.2em 0.4rem',
  input_padding: '0.8em',
  alert_padding: '0.8em 1.2rem',

  // Radius
  base_radius: '5px',

  // Shadows
  base_shadow: '0px 3px 6px 1px rgba(0, 0, 0, .2)',
  base_text_shadow: '1px 1px 5px #000',
};

export default theme;
