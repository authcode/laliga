import { LoginState } from '../../../login/states/loginState.d';
import { UserListState } from '../../../users/components/UsersList/types.d';

export interface AppState {
  readonly login: LoginState;
  readonly users: UserListState;
}
