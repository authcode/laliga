import React, { useState } from 'react';
import { fireEvent, render } from '@testing-library/react';
import Input from './Input';
import { InputProps } from './types.d';

function TextInput(props: InputProps) {
  const [value, setValue] = useState<string>('Initial value');

  const handleChange = (ev: React.FormEvent<HTMLInputElement>) => {
    ev.preventDefault();
    setValue(ev.currentTarget.value);
  };

  return (
    <Input label={props.label} aria-label="test-input" value={value} onChange={handleChange} />
  );
}

const setup = () => {
  const component = render(<TextInput label="Test input" />);
  const input = component.getByLabelText('test-input') as HTMLInputElement;
  return {
    input,
    ...component,
  };
};

describe('Input component', () => {
  test('Check initial input value and label', () => {
    const { input, getAllByText } = setup();
    const label = getAllByText('Test input').pop();
    expect(input.value).toBe('Initial value');
    expect(label).toBeInTheDocument();
  });

  test('It should change the input value', () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: 'New value' } });
    expect(input.value).toBe('New value');
  });
});
