import { UserList } from '../types/users.d';

export interface UserListState extends UserList {
  readonly loading: boolean;
  readonly error?: string;
}
