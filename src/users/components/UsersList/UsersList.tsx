import React, { useEffect } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsers, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { Alert, Button, Card, Paginator, Spinner } from '../../../common/components';
import { Table } from '../../../common/components';
import { TableColumn } from '../../../common/components/Table/types';
import { AppState } from '../../../common/store/states';
import { getUsers } from '../../actions/getUsersActions';
import { UserListState } from '../../states/usersListState';
import { USERS_PAGE_ROUTE, USERS_EDIT_ROUTE } from '../../routes';
import { UserListRouteProps, UserLocationProps } from './types';

const UsersList: React.FC = () => {
  const history = useHistory();
  const location = useLocation<UserLocationProps>();
  const { currentPage } = useParams<UserListRouteProps>();
  const numCurrentPage = Number(currentPage);
  const columns: TableColumn[] = [
    { field: 'id', title: '#ID' },
    { field: 'first_name', title: 'Nombre', noMobile: true },
    { field: 'last_name', title: 'Apellidos', noMobile: true },
    { field: 'email', title: 'E-Mail' },
    { field: 'options', title: 'Opc.', align: 'right' },
  ];
  const { data, page, total, total_pages, loading } = useSelector<AppState, UserListState>(
    (state) => state.users,
  );
  const dispatch = useDispatch();

  function handlePaginate(newPage: number): void {
    history.push(USERS_PAGE_ROUTE.replace(':currentPage', newPage.toString()));
  }

  function handleEditUser(id: number): void {
    history.push(USERS_EDIT_ROUTE.replace(':user_id', id.toString()));
  }

  useEffect(() => {
    if (data && !data.length) history.push(USERS_PAGE_ROUTE.replace(':currentPage', '1'));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    if (!data || !data?.length || page !== numCurrentPage || location.state?.success) {
      dispatch(getUsers(numCurrentPage));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage]);

  return (
    <>
      {location.state?.success && (
        <Alert type="success" style={{ textAlign: 'center' }}>
          {location.state.success}
        </Alert>
      )}
      <Card
        title={
          <h3>
            <FontAwesomeIcon icon={faUsers} />
            Usuarios
          </h3>
        }
        noPadding
      >
        {loading && <Spinner />}
        {!loading && data && data?.length > 0 && (
          <Table
            columns={columns}
            data={data.map((user) => ({
              ...user,
              options: (
                <Button
                  color="success"
                  title="Editar"
                  onClick={() => handleEditUser(user.id)}
                  sm
                  mx
                >
                  <FontAwesomeIcon icon={faPencilAlt} />
                  Editar
                </Button>
              ),
            }))}
          />
        )}
      </Card>
      <Paginator
        page={Number(currentPage)}
        totalPages={total_pages}
        totalItems={total}
        itemsName="usuario"
        onPaginate={handlePaginate}
      />
    </>
  );
};

export default UsersList;
