import * as Yup from 'yup';
import { FORMAT_EMAIL, REQUIRED_FIELD } from '../../common/utils';

export const loginValidationSchema = Yup.object().shape({
  username: Yup.string().email(FORMAT_EMAIL).required(REQUIRED_FIELD),
  password: Yup.string().required(REQUIRED_FIELD),
});
