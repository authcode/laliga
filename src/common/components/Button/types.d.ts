import { HTMLProps } from 'react';

export interface ButtonProps extends HTMLProps<HTMLButtonElement> {
  readonly color?: 'default' | 'primary' | 'success' | 'danger' | 'warning';
  readonly full?: boolean;
  readonly iconOnly?: boolean;
  readonly sm?: boolean;
  readonly mx?: boolean;
  readonly fullSm?: boolean;
}
