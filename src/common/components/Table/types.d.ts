import { CSSProperties } from 'react';

export interface TableColumn {
  readonly title: string;
  readonly field: string;
  readonly align?: 'left' | 'right' | 'center';
  readonly noTablet?: boolean;
  readonly noMobile?: boolean;
}

export interface TableData {
  readonly [key: string]: unknown;
}

export interface TableProps {
  readonly columns: Readonly<TableColumn[]>;
  readonly data: Readonly<TableData[]>;
  readonly style?: CSSProperties;
}
