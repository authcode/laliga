import { combineReducers } from 'redux';
import login from '../../../login/reducers/loginReducer';
import users from '../../../users/reducers/usersReducer';

const rootReducer = combineReducers({
  login,
  users,
});

export default rootReducer;
