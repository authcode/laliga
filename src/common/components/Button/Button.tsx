import { ButtonProps } from './types.d';
import styled from 'styled-components';

const Button = styled.button<ButtonProps>`
  background-color: ${(props) => (props.color ? props.theme[props.color] : props.theme.primary)};
  color: ${(props) => (props.color === 'default' ? props.theme.black : props.theme.white)};
  font-size: ${(props) => props.theme.font_md};
  padding: ${(props) => (props.sm ? props.theme.button_padding_sm : props.theme.button_padding)};
  border: 2px solid;
  border-color: ${(props) => (props.color ? props.theme[props.color] : props.theme.primary)};
  border-radius: ${(props) => props.theme.base_radius};
  text-decoration: none;
  outline: none;
  cursor: pointer;
  width: ${(props) =>
    props.full ? '100%' : props.iconOnly ? (props.sm ? '30px' : '36px') : 'auto'};
  margin-left: ${(props) => (props.mx ? '0.3em' : '0')};
  margin-right: ${(props) => (props.mx ? '0.3em' : '0')};
  svg {
    margin-right: ${(props) => (props.iconOnly ? '0px' : '6px')};
  }
  &:hover,
  &:disabled {
    background-color: ${(props) =>
      props.color ? props.theme[`${props.color}_hover`] : props.theme.primary_hover};
    border-color: ${(props) =>
      props.color ? props.theme[`${props.color}_hover`] : props.theme.primary_hover};
  }
  &:disabled {
    cursor: default;
  }
  @media screen and (max-width: 767px) {
    ${(props) => props.fullSm && 'width: 100%; margin-bottom: 10px'};
  }
`;

export default Button;
