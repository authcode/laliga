import React from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { LOGIN_ROUTE } from '../../../login/routes';
import { useAuth } from '../../hooks';
import { PrivateRouteProps } from './types';

const PrivateRoute: React.FC<PrivateRouteProps> = ({ children, ...props }) => {
  const { getToken } = useAuth();
  const token = getToken();

  return (
    <Route
      {...props}
      render={({ location }) =>
        token ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: LOGIN_ROUTE,
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default withRouter(PrivateRoute);
