export interface ResponseError {
  readonly error: string;
}
