import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useFormik } from 'formik';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserEdit, faSave, faTimes, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Button, Card, Input, Spinner, Alert } from '../../../common/components';
import { AppState } from '../../../common/store/states';
import { deleteUser, getUser, updateUser } from '../../services';
import { USERS_PAGE_ROUTE, USERS_ROUTE } from '../../routes';
import { UserFormValues, UserViewParams } from './types';
import { userValidationSchema } from './utils';

const StyledAvatar = styled.img`
  width: 120px;
  height: 120px;
  object-fit: cover;
  border-radius: 100%;
  margin-bottom: 15px;
`;

const UserView: React.FC = () => {
  const history = useHistory();
  const { user_id } = useParams<UserViewParams>();
  const usersPage = useSelector<AppState, Number>((state) => state.users?.page || 1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [avatar, setAvatar] = useState<string | undefined>();
  const [error, setError] = useState<string | undefined>();
  const [showConfirm, setShowConfirm] = useState<boolean>();
  const {
    handleChange,
    handleSubmit,
    setValues,
    values,
    errors,
    touched,
  } = useFormik<UserFormValues>({
    initialValues: {
      first_name: '',
      last_name: '',
      email: '',
    },
    validationSchema: userValidationSchema,
    onSubmit: async (values: UserFormValues): Promise<void> => {
      setError(undefined);
      setIsLoading(true);
      const result = await updateUser(Number(user_id), values);
      setIsLoading(false);
      if (result?.error || !result.updatedAt) {
        setError(result?.error || 'Error inesperado al guardar los datos');
        return;
      }
      history.push(USERS_PAGE_ROUTE.replace(':currentPage', usersPage.toString()), {
        success: '¡Cambios guardados con éxito!',
      });
    },
  });

  function handleCancel(): void {
    history.push(USERS_PAGE_ROUTE.replace(':currentPage', usersPage.toString()));
  }

  async function handleDelete(): Promise<void> {
    const result = await deleteUser(Number(user_id));
    if (result?.error) {
      setError(result?.error || 'Error inesperado al eliminar el usuario');
      return;
    }
    history.push(USERS_PAGE_ROUTE.replace(':currentPage', usersPage.toString()), {
      success: '¡Usuario eliminado correctamente!',
    });
  }

  async function handleGetUser(): Promise<void> {
    setIsLoading(true);
    const response = await getUser(Number(user_id));
    setIsLoading(false);
    if (response?.error) {
      handleCancel();
      return;
    }
    setValues(response?.data);
    if (response?.data?.avatar) {
      setAvatar(response?.data?.avatar);
    }
  }

  useEffect((): void => {
    if (!user_id) {
      history.push(USERS_ROUTE);
      return;
    }
    handleGetUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Card
        title={
          <h3>
            <FontAwesomeIcon icon={faUserEdit} />
            Editar Usuario
          </h3>
        }
      >
        {isLoading ? (
          <Spinner />
        ) : (
          <>
            {avatar && (
              <div style={{ textAlign: 'center' }}>
                <StyledAvatar src={avatar} alt={values.first_name} />
              </div>
            )}
            <form onSubmit={handleSubmit} style={{ maxWidth: '350px', margin: '0 auto' }}>
              <Input
                type="text"
                name="first_name"
                label="Nombre"
                onChange={handleChange}
                value={values.first_name}
                error={errors.first_name && touched.first_name ? errors.first_name : undefined}
                required
              />
              <Input
                type="text"
                name="last_name"
                label="Apellidos"
                onChange={handleChange}
                value={values.last_name}
                error={errors.last_name && touched.last_name ? errors.last_name : undefined}
                required
              />
              <Input
                type="text"
                name="email"
                label="E-Mail"
                onChange={handleChange}
                value={values.email}
                error={errors.email && touched.email ? errors.email : undefined}
                required
              />
              {showConfirm && (
                <Alert type="warning" style={{ textAlign: 'center' }}>
                  <>
                    ¿Está seguro de eliminar el usuario?
                    <div style={{ marginTop: '15px' }}>
                      <Button
                        type="button"
                        color="warning"
                        onClick={() => setShowConfirm(false)}
                        mx
                        fullSm
                      >
                        <FontAwesomeIcon icon={faTimes} />
                        No, cancelar
                      </Button>
                      <Button type="button" color="danger" onClick={handleDelete} mx fullSm>
                        <FontAwesomeIcon icon={faTrashAlt} />
                        Si, eliminar
                      </Button>
                    </div>
                  </>
                </Alert>
              )}
              {!showConfirm && error && <Alert type="danger">{error}</Alert>}
              {!showConfirm && (
                <div style={{ textAlign: 'center', marginTop: '20px' }}>
                  <Button type="button" color="default" onClick={handleCancel} mx fullSm>
                    <FontAwesomeIcon icon={faTimes} />
                    Cancelar
                  </Button>
                  <Button type="submit" color="success" mx fullSm>
                    <FontAwesomeIcon icon={faSave} />
                    Guardar
                  </Button>
                  <Button
                    type="button"
                    color="danger"
                    onClick={() => setShowConfirm(true)}
                    mx
                    fullSm
                  >
                    <FontAwesomeIcon icon={faTrashAlt} />
                    Eliminar
                  </Button>
                </div>
              )}
            </form>
          </>
        )}
      </Card>
    </>
  );
};

export default UserView;
