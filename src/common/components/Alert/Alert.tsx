import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle, faCheck, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { AlertProps } from './types.d';

const StyledAlert = styled.div<AlertProps>`
  color: ${(props) => (props.type ? props.theme[props.type] : props.theme.primary)};
  background-color: ${(props) =>
    props.type ? props.theme[`${props.type}_soft`] : props.theme.primary_soft};
  border: 1px solid;
  border-color: ${(props) => (props.type ? props.theme[props.type] : props.theme.primary)};
  border-radius: ${(props) => props.theme.base_radius};
  padding: ${(props) => props.theme.alert_padding};
  margin-bottom: 15px;
  svg {
    margin-right: 6px;
  }
`;

const Alert: React.FC<AlertProps> = ({ children, ...props }) => {
  return (
    <StyledAlert {...props}>
      {props.type === 'info' && <FontAwesomeIcon icon={faInfoCircle} />}
      {props.type === 'success' && <FontAwesomeIcon icon={faCheck} />}
      {(props.type === 'warning' || props.type === 'danger') && (
        <FontAwesomeIcon icon={faExclamationTriangle} />
      )}
      {children}
    </StyledAlert>
  );
};

export default Alert;
