import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

const StyledSpinner = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 300px;
  svg {
    color: ${(props) => props.theme.primary};
    font-size: 32px;
  }
`;

const Spinner: React.FC = () => {
  return (
    <StyledSpinner>
      <FontAwesomeIcon title="Cargando..." icon={faSpinner} spin />
    </StyledSpinner>
  );
};

export default Spinner;
