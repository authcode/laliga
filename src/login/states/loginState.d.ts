export interface LoginState {
  readonly isLoading: boolean;
  readonly error?: string;
  readonly token?: string;
}
