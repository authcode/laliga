export interface Headers {
  readonly [key: string]: Record<string, unknown>;
}
