import React from 'react';
import { render, screen } from '@testing-library/react';
import Spinner from './Spinner';

test('Render spinner', () => {
  render(<Spinner />);
  const icon = screen.getByTitle('Cargando...');
  expect(icon).toBeInTheDocument();
});
