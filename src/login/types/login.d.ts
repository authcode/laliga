export interface LoginFormValues {
  readonly username: string;
  readonly password: string;
}

export interface LoginResponse {
  readonly token: string;
}
