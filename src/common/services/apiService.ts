import { HTTPMethods, Headers, UrlParameters, ResponseError } from '../types';
import { UNEXPECTED_ERROR } from '../utils/literals';

function apiUrl(path: string): string {
  return `${process.env.REACT_APP_API_URL}/${path}`;
}

function apiFetch<T>(
  method: HTTPMethods,
  url: string,
  params?: UrlParameters,
  headers?: Headers,
): Promise<T & ResponseError> {
  const token = window?.sessionStorage
    ? window.sessionStorage.getItem(process.env.REACT_APP_AUTH_TOKEN || 'authToken')
    : undefined;
  return fetch(apiUrl(url), {
    method,
    headers: {
      'Content-Type': 'application/json',
      ...(token && { Authorization: `Bearer ${token}` }),
      ...(headers && headers),
    },
    body: params ? JSON.stringify(params) : undefined,
  })
    .then(async (response: Response) => {
      if (method !== HTTPMethods.DELETE) {
        return await response.json();
      } else if (!response.ok) {
        return { error: UNEXPECTED_ERROR };
      }
    })
    .catch(() => {
      return { error: UNEXPECTED_ERROR };
    });
}

export function apiGet<T>(url: string): Promise<T & ResponseError> {
  return apiFetch<T>(HTTPMethods.GET, url);
}

export function apiPost<T>(url: string, params?: UrlParameters): Promise<T & ResponseError> {
  return apiFetch<T>(HTTPMethods.POST, url, params);
}

export function apiPut<T>(url: string, params?: UrlParameters): Promise<T & ResponseError> {
  return apiFetch<T>(HTTPMethods.PUT, url, params);
}

export function apiDelete<T>(url: string): Promise<T & ResponseError> {
  return apiFetch<T>(HTTPMethods.DELETE, url);
}
