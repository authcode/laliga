export interface UrlParameters {
  readonly [key: string]: unknown;
}
