import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUnlockAlt } from '@fortawesome/free-solid-svg-icons';
import { Button, Card, Input } from '../common/components';
import { AppState } from '../common/store/states';
import logo from '../assets/logo.svg';
import { useAuth } from '../common/hooks';
import { login } from './actions/loginActions';
import { setContextToken } from './actions/setTokenActions';
import { LoginFormValues } from './types/login';
import { Redirect } from 'react-router-dom';
import { loginValidationSchema } from './utils';
import { LoginState } from './states/loginState';
import Alert from '../common/components/Alert/Alert';
import { USERS_ROUTE } from '../users/routes';

const Login: React.FC = () => {
  const loginState = useSelector<AppState, LoginState>((state) => state.login);
  const dispatch = useDispatch();
  const { getToken, setToken } = useAuth();

  const { handleChange, handleSubmit, values, errors, touched } = useFormik<LoginFormValues>({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema: loginValidationSchema,
    onSubmit: (values: LoginFormValues) => {
      dispatch(login(values));
    },
  });

  useEffect(() => {
    if (loginState.token !== undefined) {
      setToken(loginState.token as string);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loginState.token]);

  useEffect(() => {
    const contextToken = getToken();
    if (contextToken !== undefined) {
      dispatch(setContextToken(contextToken as string));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return loginState.token !== undefined ? (
    <Redirect to={USERS_ROUTE} />
  ) : (
    <Card maxWidth={350} style={{ margin: '25px auto' }}>
      <div style={{ textAlign: 'center', marginBottom: '15px' }}>
        <img src={logo} alt="LaLiga" style={{ width: '65%' }} />
      </div>
      {loginState.error && <Alert type="danger">{loginState.error}</Alert>}
      <form onSubmit={handleSubmit}>
        <Input
          type="text"
          name="username"
          label="E-Mail"
          onChange={handleChange}
          value={values.username}
          error={errors.username && touched.username ? errors.username : undefined}
          required
        />
        <Input
          type="password"
          name="password"
          label="Contraseña"
          onChange={handleChange}
          value={values.password}
          error={errors.password && touched.password ? errors.password : undefined}
          required
        />
        <Button type="submit" disabled={loginState.isLoading} full>
          <FontAwesomeIcon icon={faUnlockAlt} />
          Acceder
        </Button>
      </form>
    </Card>
  );
};

export default Login;
