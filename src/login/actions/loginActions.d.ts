import { Action } from 'redux';
import { LoginFormValues } from '../types/login.d';
import { LoginState } from '../types/login.d';

export const LOGIN_CALLED = 'login/login_called';
export const LOGIN_SUCEEDED = 'login/login_suceeded';
export const LOGIN_FAILED = 'login/login_failed';

export interface LoginCalledAction extends Action<typeof LOGIN_CALLED> {
  readonly payload: LoginFormValues;
}

export interface LoginSuceededAction extends Action<typeof LOGIN_SUCEEDED> {
  readonly loginState: LoginState;
}

export interface LoginFailedAction extends Action<typeof LOGIN_FAILED> {
  readonly error: string;
}

export type LoginActions = LoginCalledAction | LoginSuceededAction | LoginFailedAction;
