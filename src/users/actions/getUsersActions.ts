import { GET_USERS_CALLED } from './getUsersActions.d';

export function getUsers(page: number) {
  return {
    type: GET_USERS_CALLED,
    page,
  };
}
