import React from 'react';
import styled from 'styled-components';

const StyledFooter = styled.footer`
  text-align: center;
  color: ${(props) => props.theme.white};
  text-shadow: ${(props) => props.theme.base_text_shadow};
`;

const Footer: React.FC = () => {
  const date: Date = new Date();
  return <StyledFooter>&copy; {date.getFullYear()} LaLiga - All rights reserved</StyledFooter>;
};

export default Footer;
