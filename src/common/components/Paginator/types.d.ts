import { HTMLAttributes } from 'react';
export interface PaginatorProps extends HTMLAttributes<HTMLDivElement> {
  readonly page: number;
  readonly totalPages: number;
  readonly totalItems: number;
  // eslint-disable-next-line no-unused-vars
  readonly onPaginate: (page: number) => void;
  readonly itemsName?: string;
}
