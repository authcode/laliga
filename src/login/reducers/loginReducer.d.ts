import { LoginActions } from '../actions/loginActions.d';
import { SetTokenActions } from '../actions/setTokenActions.d';

export type LoginReducerActions = LoginActions | SetTokenActions;
