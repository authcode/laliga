import { SagaIterator } from 'redux-saga';
import { call, put, takeLatest } from 'redux-saga/effects';
import {
  GetUsersCalledAction,
  GET_USERS_CALLED,
  GET_USERS_FAILED,
  GET_USERS_SUCEEDED,
} from '../actions/getUsersActions.d';
import { getUsers as getUsersService } from '../services';

function* getUsers(action: GetUsersCalledAction): SagaIterator {
  const result = yield call(getUsersService, action.page);
  if ('error' in result) {
    yield put({ type: GET_USERS_FAILED, error: result.error });
  } else {
    yield put({
      type: GET_USERS_SUCEEDED,
      usersList: {
        ...result,
        loading: false,
      },
    });
  }
}

function* getUsersSaga() {
  yield takeLatest(GET_USERS_CALLED, getUsers);
}

export default getUsersSaga;
