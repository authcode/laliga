export interface UserListRouteProps {
  readonly currentPage?: string;
}

export interface UserLocationProps {
  readonly success?: string;
}
