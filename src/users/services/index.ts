import { apiDelete, apiGet, apiPut } from '../../common/services';
import { ResponseError } from '../../common/types';
import { UserFormValues } from '../components/UserView/types';
import { UserList, UserResponse, UserUpdateResponse } from '../types/users.d';

const USERS_URL = `users`;

export function getUsers(page?: number): Promise<UserList & ResponseError> {
  const url = `${USERS_URL}${page !== undefined && `?page=${page}`}`;
  return apiGet<UserList>(url);
}

export function getUser(id: number): Promise<UserResponse & ResponseError> {
  const url = `${USERS_URL}/${id}`;
  return apiGet<UserResponse>(url);
}

export function updateUser(
  id: number,
  values: UserFormValues,
): Promise<UserUpdateResponse & ResponseError> {
  const url = `${USERS_URL}/${id}`;
  return apiPut<UserUpdateResponse>(url, { ...values });
}

export function deleteUser(id: number): Promise<ResponseError> {
  const url = `${USERS_URL}/${id}`;
  return apiDelete(url);
}
