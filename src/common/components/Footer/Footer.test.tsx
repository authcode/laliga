import React from 'react';
import { render, screen } from '@testing-library/react';
import Footer from './Footer';

test('Render card', () => {
  render(<Footer />);
  const text = screen.getByText(/LaLiga/i);
  expect(text).toBeInTheDocument();
});
