import React, { useState } from 'react';
import { fireEvent, render } from '@testing-library/react';
import Button from './Button';
import { ButtonProps } from './types.d';

function TestButton(props: ButtonProps) {
  const [text, setText] = useState<string>(props.children as string);

  const handleClick = () => {
    setText('New text');
  };

  return (
    <Button type="button" aria-label="test-button" onClick={handleClick}>
      {text}
    </Button>
  );
}

const setup = () => {
  const component = render(<TestButton type="button">Initial text</TestButton>);
  const button = component.getByLabelText('test-button') as HTMLButtonElement;
  return {
    button,
    ...component,
  };
};

describe('Button component', () => {
  test('Check initial button text', () => {
    const { button } = setup();
    expect(button.textContent).toBe('Initial text');
  });

  test('It should change the button text', () => {
    const { button } = setup();
    fireEvent.click(button);
    expect(button.textContent).toBe('New text');
  });
});
