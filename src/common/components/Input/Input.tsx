import React from 'react';
import styled from 'styled-components';
import { InputProps } from './types';

const StyledInput = styled.input<InputProps>`
  width: ${(props) => (props.full ? '100%' : 'auto')};
  border: 1px solid ${(props) => props.theme.white_2};
  border-radius: ${(props) => props.theme.base_radius};
  padding: ${(props) => props.theme.input_padding};
  margin-bottom: ${(props) => (props?.error ? '3px' : '14px')};
  width: calc(100% - (2 * 0.8em));
  outline: none !important;
  border: 2px solid ${(props) => props.theme.white_2};
  border-color: ${(props) => (props?.error ? props.theme.danger : props.theme.white_2)};
  &:focus {
    border-color: ${(props) => (props?.error ? props.theme.danger : props.theme.primary)};
  }
`;

const StyledLabel = styled.label`
  font-size: 0.8em;
  font-weight: 600;
  display: block;
  margin-bottom: 5px;
`;

const StyledError = styled.div`
  color: ${(props) => props.theme.danger};
  font-size: ${(props) => props.theme.font_sm};
  margin-bottom: '14px';
`;

const StyledRequired = styled.span`
  color: ${(props) => props.theme.danger};
  display: inline-block;
  margin-left: 5px;
`;

const Input: React.FC<InputProps> = ({ label, required, ...props }) => {
  return (
    <div>
      {label && (
        <StyledLabel htmlFor={props.id}>
          {label}
          {required && <StyledRequired>*</StyledRequired>}
        </StyledLabel>
      )}
      <StyledInput {...props} />
      {props.error && <StyledError style={{ marginBottom: '12px' }}>{props.error}</StyledError>}
    </div>
  );
};

export default Input;
