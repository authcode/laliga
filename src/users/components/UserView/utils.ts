import * as Yup from 'yup';
import { FORMAT_EMAIL, REQUIRED_FIELD } from '../../../common/utils';

export const userValidationSchema = Yup.object().shape({
  first_name: Yup.string().required(REQUIRED_FIELD),
  last_name: Yup.string().required(REQUIRED_FIELD),
  email: Yup.string().email(FORMAT_EMAIL).required(REQUIRED_FIELD),
});
